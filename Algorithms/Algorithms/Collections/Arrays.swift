//
//  Arrays.swift
//  Algorithms
//
//  Created by Anton Lomakin on 02/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public protocol OTUSCollection {
    
    associatedtype Item
    
    var size: Int { get }
    
    func add(_ item: Item)
    func get(at index: Int) -> Item
//    func remove(at index: Int) -> Item
}

extension OTUSCollection {
    
    public func resize(_ array: [Item], delta: Int) -> [Item] {
        var newArray: [Item] = []
        for i in stride(from: 0, through: array.count, by: 1) {
            if i < array.count {
                newArray.append(array[i])
            }
        }
        return newArray
    }
}

public class SingleArray<T>: OTUSCollection {
    public typealias Item = T
    
    private var tarray = [T?](repeating: nil, count: 0)
    
    private var array: [T] = []
    
//    public required init() {
//    }
    
    public var size: Int { array.count }
    
    public func add(_ item: T) {
        array = resize(array, delta: 1)
        array.append(item)
    }
    
    public func get(at index: Int) -> T {
        return array[index]
    }
    
//    public func remove(at index: Int) -> T {
//
//    }
}

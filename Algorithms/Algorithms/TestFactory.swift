//
//  TestFactory.swift
//  Algorithms
//
//  Created by Anton Lomakin on 23/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public protocol Executable: class {
    func executeTask(for data: [ExampleTest])
}

public protocol Factoriable: class {
    func startFabric(for testsPath: String)
}

public protocol TaskInterface {
    
    var testPath: String { get }
    var executedTask: Executable? { get }
}

public final class TestFactory {
    
    private enum ThemeDialog: String, CaseIterable {
        
        case strings = "Работа со строками"
        case lucky_tiket = "Счастливые билеты"
        case algebraic = "Алгебраические алгоритмы"
        case bitboard = "Битовые шахматы"
        case cancel = "Назад"
        
        init?(rawValue: String) {
            guard let intValue = Int(rawValue) else { return nil }
            switch intValue {
            case 0: self = .strings
            case 1: self = .lucky_tiket
            case 2: self = .algebraic
            case 3: self = .bitboard
            default: self = .cancel
            }
        }
        
        var taskPath: String {
            switch self {
            case .algebraic: return "/AlgebraicAlgorithms"
            case .bitboard: return "/BITS"
            default: return ""
            }
        }
        
        var themeFactory: Factoriable? {
            switch self {
            case .strings: return StringFactory()
            case .lucky_tiket: return LukyTicketFactory()
            case .algebraic: return AlgebraicFactory()
            case .bitboard: return BitboardChessFactory()
            default: return nil
            }
        }
    }
    
    private var dialogValue: ThemeDialog?
    
    // MARK: - Private
    
    private func startThemeFactory(for result: String?, in directory: String) {
        guard let string = result, let dialogCase = ThemeDialog(rawValue: string) else { return }
        if let themeFactory = dialogCase.themeFactory {
            print("\nВыбранная тема - \(dialogCase.rawValue).")
            themeFactory.startFabric(for: directory + dialogCase.taskPath)
        } else {
            dialogValue = dialogCase
        }
    }
    
    // MARK: - Public
    
    public func showDialog(with directory: String) {
        while dialogValue != .cancel {
            print("\nВыберите тему:")
            for (index, value) in ThemeDialog.allCases.enumerated() {
                print("\t\(index): \(value.rawValue)")
            }
            print("\nТема №:")
            startThemeFactory(for: readLine(), in: directory)
        }
    }
}


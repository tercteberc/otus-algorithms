//
//  GCF.swift
//  Algorithms
//
//  Created by Anton Lomakin on 24/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//
import Foundation

//1. Алгоритм Евклида поиска НОД макс. 4 байта
//1a. Через вычитание
//+1 байт 1b. Через остаток
//+1 байт 1c. Через битовые операции
//+2 байт Составить сравнительную таблицу времени работы алгоритмов для разных начальных данных.

fileprivate struct Result {
    let title: String
    let results: [ResultGCF]
}

fileprivate struct ResultGCF {
    let numberOfTest: Int // порядковый номер теста
    let resultBool: String // пройден ли тест?
    let resultGCF: Int // результат теста (числовое значение найденного НОД)
    let time: Double // Время выполнения теста
}

// Greatest Common Factor
public final class GCF {
    
    // MARK: - Через вычитание
    
    private func subtraction(data: [ExampleTest]) -> [ResultGCF] {
        var result: [ResultGCF] = []
        for testData in data {
            guard var a = Int(testData.data.inputs.first ?? ""),
                var b = Int(testData.data.inputs.last ?? ""),
                let outputInt = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            let startTime = CFAbsoluteTimeGetCurrent()
            while a != b {
                if a > b {
                    a = a - b
                } else {
                    b = b - a
                }
            }
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(a == outputInt).uppercased(),
                                resultGCF: a,
                                time: timeElapsed))
        }
        return result
    }
    
    // MARK: - Через остаток
    // For Loop
    private func residueLine(data: [ExampleTest]) -> [ResultGCF] {
        var result: [ResultGCF] = []
        for testData in data {
            guard var a = Int(testData.data.inputs.first ?? ""),
                var b = Int(testData.data.inputs.last ?? ""),
                let outputInt = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            let startTime = CFAbsoluteTimeGetCurrent()
            while a != 0, b != 0 {
                if a > b {
                    a = a % b
                } else {
                    b = b % a
                }
            }
            let res = a + b
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(res == outputInt).uppercased(),
                                resultGCF: res,
                                time: timeElapsed))
        }
        return result
    }
    // Рекурсивно
    private func residueRecursion(data: [ExampleTest]) -> [ResultGCF] {
        var result: [ResultGCF] = []
        for testData in data {
            guard let a = Int(testData.data.inputs.first ?? ""),
                let b = Int(testData.data.inputs.last ?? ""),
                let outputInt = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            let startTime = CFAbsoluteTimeGetCurrent()
            let res = calcResideRecurs(a, b)
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(res == outputInt).uppercased(),
                                resultGCF: res,
                                time: timeElapsed))
        }
        return result
    }
    
    private func calcResideRecurs(_ a: Int, _ b: Int) -> Int {
        if b == 0 { return a }
        return calcResideRecurs(b, a % b)
    }
    
    // MARK: - Битовым сдвигом
    private func bitwiseRecursion(data: [ExampleTest]) -> [ResultGCF] {
        var result: [ResultGCF] = []
        for testData in data {
            guard let a = Int(testData.data.inputs.first ?? ""),
                let b = Int(testData.data.inputs.last ?? ""),
                let outputInt = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            let startTime = CFAbsoluteTimeGetCurrent()
            let res = calcBitwizeRecurs(a, b)
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(res == outputInt).uppercased(),
                                resultGCF: res,
                                time: timeElapsed))
        }
        return result
    }
    
    private func calcBitwizeRecurs(_ a: Int, _ b: Int) -> Int {
        if a == b || b == 0 { return a }
        if a == 0 { return b }
        if (~a & 1) == 1 { // a четное
            if (b & 1) == 1 { // b нечетное
                return calcBitwizeRecurs(a >> 1, b)
            } else { // b четное
                return calcBitwizeRecurs(a >> 1, b >> 1) << 1
            }
        }
        if (~b & 1) == 1 { // a нечетное, b четное
            return calcBitwizeRecurs(a, b >> 1)
        }
        // уменьшить больший аргумент
        if a > b {
            return calcBitwizeRecurs((a - b) >> 1, b)
        }
        return calcBitwizeRecurs((b - a) >> 1, a)
    }
}

extension GCF: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        var results: [Result] = []
        results.append(.init(title: "\t\t\t\t\t\tЧерез вычитание", results: subtraction(data: data)))
        results.append(.init(title: "\t\t\t\tЧерез остаток. Циклом for", results: residueLine(data: data)))
        results.append(.init(title: "\t\t\t\tЧерез остаток. Рекурсивно", results: residueRecursion(data: data)))
        results.append(.init(title: "\t\t\t\tБитовый сдвиг. Рекурсивно", results: bitwiseRecursion(data: data)))
        let format = "\t%@\t|\t%@\t|\t%@\t|\t%@"
        print("--------------------------------------------------------------")
        print(String(format: format, "№", "Верно?", "Результат", "Затрачено времени"))
        for result in results {
            print("--------------------------------------------------------------")
            print(result.title)
            print("--------------------------------------------------------------")
            for data in result.results {
                let time = String(format: "%.20f", data.time)
                print(String(format: format, "\(data.numberOfTest)", data.resultBool, "\t\(data.resultGCF)\t", time))
            }
        }
        print("--------------------------------------------------------------")
    }
}

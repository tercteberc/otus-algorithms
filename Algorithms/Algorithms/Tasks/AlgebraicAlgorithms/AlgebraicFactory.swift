//
//  AlgebraicFactory.swift
//  Algorithms
//
//  Created by Anton Lomakin on 01/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public struct AlgebraicReadRules: ReadRules {
    
    private struct Rule: ReadRule {
        var rule: String
        
        func processing(string: String) -> [String] {
            var comp = components(of: string, separetedBy: self.rule)
            if let last = comp.last {
                comp.removeLast()
                let normalLast = last.replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\r", with: "")
                comp.append(normalLast)
            }
            return comp
        }
    }
    
    public let inputRule: ReadRule = Rule(rule: " ")
    public let outputRule: ReadRule = Rule(rule: " ")
}

public final class AlgebraicFactory: Factoriable {
    
    private enum Task: String, CaseIterable, TaskInterface {
        
        case gcf = "Поиск НОД"
        case spn = "Поиск простых чисел"
        case cancel = "Назад"
        
        init?(rawValue: String) {
            guard let intValue = Int(rawValue) else { return nil }
            switch intValue {
            case 0: self = .gcf
            case 1: self = .spn
            default: self = .cancel
            }
        }
        
        var testPath: String {
            switch self {
            case .gcf: return "/GCF"
            case .spn: return "/SPN"
            default: return ""
            }
        }
        
        var executedTask: Executable? {
            switch self {
            case .gcf: return GCF()
            case .spn: return SPN()
            default: return nil
            }
        }
    }
    
    private var currentTask: Task?
    
    // MARK: - Private
    
    private func showTasksDialog(for testsPath: String) {
        while currentTask != .cancel {
            print("\nДоступные задания для тестов:")
            for (index, value) in Task.allCases.enumerated() {
                print("\t\(index): \(value.rawValue)")
            }
            print("\nТестировать будем задание №: ")
            startTask(for: readLine(), in: testsPath)
        }
    }
    
    private func startTask(for result: String?, in testsPath: String) {
        guard let string = result, let task = Task(rawValue: string) else { return }
        if let test = task.executedTask {
            print("\n\n\(task.rawValue). Результат тестов:")
            let path = testsPath + task.testPath
            test.executeTask(for: TestExampleReader.data(for: path, by: AlgebraicReadRules()))
        } else {
            currentTask = task
        }
    }
    
    // MARK: Public
    
    public func startFabric(for testsPath: String) {
        showTasksDialog(for: testsPath)
    }
}

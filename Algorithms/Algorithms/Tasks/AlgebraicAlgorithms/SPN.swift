//
//  SPN.swift
//  Algorithms
//
//  Created by Anton Lomakin on 24/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

//3. Алгоритмы поиска кол-ва простых чисел до N макс. 6 байт
//3a. Через перебор делителей.
//+1 байт 3b. Несколько оптимизаций перебора делителей, с использованием массива.
//+1 байт 3c. Решето Эратосфена со сложностью O(n log log n).
//+1 байт 3d. Решето Эратосфена с оптимизацией памяти: битовая матрица, по 32 значения в одном int
//+1 байт 3e. Решето Эратосфена со сложностью O(n)
//+2 байт Составить сравнительную таблицу времени работы алгоритмов для разных начальных данных.

fileprivate struct Result {
    let title: String
    let results: [ResultSPN]
}

fileprivate struct ResultSPN {
    let numberOfTest: Int // порядковый номер теста
    let resultBool: String // пройден ли тест?
    let maxNumber: Int // максимальное число, до которого нужно искать простые числа
    let resultSPN: Int // результат теста (кол-во найденных простых чисел)
    let time: Double // Время выполнения теста
}

// Search Prime Numbers
public final class SPN {
    
    private enum BrutforceMethod {
        case notOptimization // просто перебор
        case optimization1 // перебор, выкидывая ненужные делители
        case optimization2 // перебор, с поиском до корня из искомого числа
        case optimization3 // перебор, с поиском до корня из искомого числа и запоминая число
    }
    
    private enum EratosthenesMethod {
        case original // стандартный алгоритм "решето Эратосфена"
    }
    
    // MARK: - Brute Force
    
    private func bruteforceDividers(for data: [ExampleTest], method: BrutforceMethod) -> [ResultSPN] {
        var result: [ResultSPN] = []
        for testData in data {
            guard let maxN = Int(testData.data.inputs.first ?? ""),
                let outputValue = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            var count = 0
            let startTime = CFAbsoluteTimeGetCurrent()
            switch method {
            case .notOptimization:
                for number in 2 ... maxN {
                    if bruteforcePrime1(number: number) {
                        count += 1
                    }
                }
            case .optimization1:
                for number in 2 ... maxN {
                    if bruteforcePrime2(number: number) {
                        count += 1
                    }
                }
            case .optimization2:
                for number in 2 ... maxN {
                    if bruteforcePrime3(number: number) {
                        count += 1
                    }
                }
            case .optimization3:
                var primes: [Int] = []
                for number in 2 ... maxN {
                    if bruteforcePrime4(number: number, primes: primes) {
                        primes.append(number)
                        count += 1
                    }
                }
            }
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(count == outputValue).uppercased(),
                                maxNumber: maxN,
                                resultSPN: count,
                                time: timeElapsed))
        }
        return result
    }
    
    // BrutforceMethod.notOptimization
    private func bruteforcePrime1(number: Int) -> Bool {
        var res = 0
        for num in 1 ... number {
            if number % num == 0 {
                res += 1
            }
        }
        return res == 2
    }
    
    // BrutforceMethod.optimization1
    private func bruteforcePrime2(number: Int) -> Bool {
        if number == 2 { return true }
        if number % 2 == 0 { return false }
        for num in stride(from: 3, through: number, by: 2) {
            if num != number, number % num == 0 {
                return false
            }
        }
        return true
    }
    
    // BrutforceMethod.optimization2
    private func bruteforcePrime3(number: Int) -> Bool {
        if number == 2 { return true }
        if number % 2 == 0 { return false }
        var num = 3
        while num * num <= number {
            if number % num == 0 { return false }
            num += 2
        }
        return true
    }
    
    // BrutforceMethod.optimization3
    private func bruteforcePrime4(number: Int, primes: [Int]) -> Bool {
        if number == 2 { return true }
        if number % 2 == 0 { return false }
        var num = 0
        while primes[num] * primes[num] <= number {
            if number % primes[num] == 0 { return false }
            num += 1
        }
        return true
    }
    
    // MARK: - Sieve Of Eratosthenes
    
    private func sieveOfEratosthenes(for data: [ExampleTest]) -> [ResultSPN] {
        var result: [ResultSPN] = []
        for testData in data {
            guard let maxN = Int(testData.data.inputs.first ?? ""),
                let outputValue = Int(testData.data.outputs.first ?? "") else {
                continue
            }
            var p = 2
            let maxStep = Int(Double(maxN).squareRoot())
            var steps = [Int](p...maxStep)
            var values = [Int](p...maxN)
            let startTime = CFAbsoluteTimeGetCurrent()
            while steps.count > 0 {
                values.removeAll(where: { $0 != p && $0 % p == 0 })
                steps.removeAll(where: { $0 % p == 0 })
                if let step = steps.first {
                    p = step
                }
            }
            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(values.count == outputValue).uppercased(),
                                maxNumber: maxN,
                                resultSPN: values.count,
                                time: timeElapsed))
        }
        return result
    }
}

extension SPN: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        var results: [Result] = []
        results.append(.init(title: "\t\t\t\t\t\t\tПеребор делителей",
                             results: bruteforceDividers(for: data, method: .notOptimization)))
        results.append(.init(title: "\t\t\t\t\tПеребор делителей, выкидывая ненужные",
                             results: bruteforceDividers(for: data, method: .optimization1)))
        results.append(.init(title: "\t\t\t\t\t\tПеребор делителей, до корня",
                             results: bruteforceDividers(for: data, method: .optimization2)))
        results.append(.init(title: "\t\t\t\t\tПеребор делителей, до корня с кэшем",
                             results: bruteforceDividers(for: data, method: .optimization3)))
        results.append(.init(title: "\t\t\t\t\t\t\tРешето Эратосфена", results: sieveOfEratosthenes(for: data)))
        let format = "\t%@\t|\t%@\t|\t\t%@\t\t|\t%@\t|\t%@"
        print("-----------------------------------------------------------------------------")
        print(String(format: format, "№", "Верно?", "Max N", "Результат", "Затрачено времени"))
        for result in results {
            print("-----------------------------------------------------------------------------")
            print(result.title)
            print("-----------------------------------------------------------------------------")
            for data in result.results {
                let time = String(format: "%.10f", data.time)
                let maxNumber = String(data.maxNumber).count < 4 ? "\(data.maxNumber)\t" : "\(data.maxNumber)"
                let resultSPN = String(data.resultSPN).count < 4 ? "\t\(data.resultSPN)\t\t" : "\t\(data.resultSPN)\t"
                print(String(format: format, "\(data.numberOfTest)", data.resultBool, maxNumber, resultSPN, time))
            }
        }
        print("-----------------------------------------------------------------------------")
    }
}

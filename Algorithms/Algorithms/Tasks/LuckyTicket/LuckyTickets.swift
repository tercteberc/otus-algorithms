//
//  LuckyTickets.swift
//  Algorithms
//
//  Created by Anton Lomakin on 24/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

// Алгоритм поиска кол-ва счастливых билетов
public final class LuckyTickets {
    
    private var n = 0
    private var result = 0
    
    private func calculateLuckyTicket(testData: ExampleTest) {
        guard let value = Int(testData.data.inputs.first ?? ""),
            let resultTest = Int(testData.data.outputs.first ?? "") else { return }
        n = value
        result = 0
        calculate(nextN: 1, sumLeft: 0, sumRight: 0)
        if result == resultTest {
            print("№\(testData.numberOfTest): True. Result = \(result)")
        } else {
            print("№\(testData.numberOfTest): False. Result = \(result)")
        }
    }
    
    // Рекурсия
    private func calculate(nextN: Int, sumLeft: Int, sumRight: Int) {
        guard nextN <= n * 2  else {
            if sumLeft == sumRight {
                result += 1
            }
            return
        }
        for i in 0 ... 9 {
            if nextN <= n {
                calculate(nextN: nextN + 1, sumLeft: sumLeft + i, sumRight: sumRight)
            } else {
                calculate(nextN: nextN + 1, sumLeft: sumLeft, sumRight: sumRight + i)
            }
        }
    }
}

extension LuckyTickets: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        for testData in data {
            calculateLuckyTicket(testData: testData)
        }
    }
}

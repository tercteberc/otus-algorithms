//
//  StringFactory.swift
//  Algorithms
//
//  Created by Anton Lomakin on 01/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public struct StringLenghtReadRules: ReadRules {
    
    private struct Rule: ReadRule {
        var rule: String
    }
    
    public let inputRule: ReadRule = Rule(rule: "\r\n")
    public let outputRule: ReadRule = Rule(rule: "\r\n")
}

public final class StringFactory: Factoriable {
    
    private enum Task: String, CaseIterable, TaskInterface {        
        
        case lenght = "Подсчет длины строки"
        case cancel = "Назад"
        
        init?(rawValue: String) {
            guard let intValue = Int(rawValue) else { return nil }
            switch intValue {
            case 0: self = .lenght
            default: self = .cancel
            }
        }
        
        var testPath: String {
            switch self {
            case .lenght: return "/String"
            default: return ""
            }
        }
        
        var executedTask: Executable? {
            switch self {
            case .lenght: return StringLenght()
            default: return nil
            }
        }
    }
    
    private var currentTask: Task?
    
    // MARK: - Private
    
    private func showTasksDialog(for testsPath: String) {
        while currentTask != .cancel {
            print("Доступные задания для тестов:")
            for (index, value) in Task.allCases.enumerated() {
                print("\t\(index): \(value.rawValue)")
            }
            print("\nТестировать будем задание №:")
            startTask(for: readLine(), in: testsPath)
        }
    }
    
    private func startTask(for result: String?, in testsPath: String) {
        guard let string = result, let task = Task(rawValue: string) else { return }
        if let test = task.executedTask {
            print("\n\n\(task.rawValue). Результат тестов:")
            let path = testsPath + task.testPath
            test.executeTask(for: TestExampleReader.data(for: path, by: StringLenghtReadRules()))
        } else {
            currentTask = task
        }
    }
    
    // MARK: Public
    
    public func startFabric(for testsPath: String) {
        showTasksDialog(for: testsPath)
    }
}

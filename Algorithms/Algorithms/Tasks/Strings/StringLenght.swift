//
//  StringLenght.swift
//  Algorithms
//
//  Created by Anton Lomakin on 23/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public final class StringLenght {
    
    private func calculateLenght(testData: ExampleTest) {
        if let count = Int(testData.data.outputs.first ?? ""), testData.data.inputs.first?.count == count {
            print("№\(testData.numberOfTest): True")
        } else {
            print("№\(testData.numberOfTest): False")
        }
    }
}

extension StringLenght: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        for testData in data {
            calculateLenght(testData: testData)
        }
    }
}

//
//  ChessFigures.swift
//  Algorithms
//
//  Created by Anton Lomakin on 01/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

typealias ChessFigure = ChessFigureInterface

fileprivate enum Constants {
    static let noneA: UInt64 = 0xFeFeFeFeFeFeFeFe;
    static let noneAB: UInt64 = 0xFcFcFcFcFcFcFcFc;
    static let noneH: UInt64 = 0x7f7f7f7f7f7f7f7f;
    static let noneGH: UInt64 = 0x3f3f3f3f3f3f3f3f;
}

public protocol ChessFigureInterface: class {
    
    var likelyPositions: [UInt64] { get }
    var variables: UInt64 { get }
    var variablesCount: Int { get }
    
    init(currentPosition: UInt64)
}

extension ChessFigureInterface {
    
    public var variables: UInt64 {
        var result: UInt64 = 0
        likelyPositions.forEach({
            result = result | $0
        })
        return result
    }
    
    public var variablesCount: Int {
        var result = 0
        var mask = variables
        while mask > 0 {
            result += 1
            mask &= mask - 1
        }
        return result
    }
}

// Фигура короля
public final class KingFigure: ChessFigure {
    
    public var likelyPositions: [UInt64]
    
    public required init(currentPosition: UInt64) {
        likelyPositions = [
            ((currentPosition & Constants.noneA) >> 1), // Клетка слева
            ((currentPosition & Constants.noneA) >> 9), // Клетка снизу слева
            (currentPosition >> 8),                     // Клетка снизу
            ((currentPosition & Constants.noneH) >> 7), // Клетка справа снизу
            ((currentPosition & Constants.noneH) << 1), // Клетка справа
            ((currentPosition & Constants.noneH) << 9), // Клетка справа сверху
            (currentPosition << 8),                     // Клетка сверху
            ((currentPosition & Constants.noneA) << 7)  // Клетка слева сверху
        ]
    }
}

// Фигура коня
public final class KnightFigure: ChessFigure {
    
    public var likelyPositions: [UInt64]
    
    public required init(currentPosition: UInt64) {
        likelyPositions = [
            ((currentPosition & Constants.noneGH) >> 6), // Клетка снизу справа на 4 часа
            ((currentPosition & Constants.noneH) >> 15), // Клетка снизу справа на 5 часов
            ((currentPosition & Constants.noneA) >> 17), // Клетка снизу слева на 7 часов
            ((currentPosition & Constants.noneAB) >> 10), // Клетка справа слева на 8 часов
            ((currentPosition & Constants.noneAB) << 6), // Клетка слева сверху на 10 часов
            ((currentPosition & Constants.noneA) << 15), // Клетка слева сверху на 11 часов
            ((currentPosition & Constants.noneH) << 17), // Клетка справа сверху на 1 час
            ((currentPosition & Constants.noneGH) << 10), // Клетка справа сверху на 2 часа
        ]
    }
}

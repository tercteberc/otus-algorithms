//
//  BitboardWalks.swift
//  Algorithms
//
//  Created by Anton Lomakin on 01/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public struct BitboardWalksReadRules: ReadRules {
    
    private struct Rule: ReadRule {
        var rule: String
        
        func processing(string: String) -> [String] {
            var comp = components(of: string, separetedBy: self.rule)
            if let last = comp.last {
                comp.removeLast()
                let normalLast = last.replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\r", with: "")
                comp.append(normalLast)
            }
            return comp
        }
    }
    
    public let inputRule: ReadRule = Rule(rule: " ")
    public let outputRule: ReadRule = Rule(rule: " ")
}

fileprivate struct Result {
    let numberOfTest: Int // порядковый номер теста
    let resultBool: String // пройден ли тест?
    let resultBits: UInt64 // битовая маска, куда может пойти король
    let steps: Int // Кол-во доступных ходов
}

// Класс реализующий прогулки битовх шахматных фигур
public final class BitboardWalks {
    
    public enum Figure {
        case king
        case knight
        
        fileprivate func figure(for position: UInt64) -> ChessFigure {
            switch self {
            case .king: return KingFigure(currentPosition: position)
            case .knight: return KnightFigure(currentPosition: position)
            }
        }
    }
    
    private let currentFigure: Figure
    
    required init(fugire: Figure) {
        currentFigure = fugire
    }
    
    private func calculate(data: [ExampleTest]) -> [Result] {
        var result: [Result] = []
        for testData in data {
            guard let boardPoint = Int(testData.data.inputs.first ?? ""),
                let count = UInt64(testData.data.outputs.first ?? ""),
                let bitsResult = UInt64(testData.data.outputs.last ?? "") else { continue }
            let kingResults = calculateFigureValues(for: UInt64(boardPoint))
            let valid = (kingResults.variablesCount == count) && (kingResults.bitsValue == bitsResult)
            result.append(.init(numberOfTest: testData.numberOfTest,
                                resultBool: String(valid).uppercased(),
                                resultBits: kingResults.bitsValue,
                                steps: kingResults.variablesCount))
        }
        return result
    }
    
    private func calculateFigureValues(for value: UInt64) -> (bitsValue: UInt64, variablesCount: Int) {
        let position: UInt64 = 1 << value // Позиция короля на шахматной доске в битах
        let figure = currentFigure.figure(for: UInt64(position))
        return (figure.variables, figure.variablesCount)
    }
}

extension BitboardWalks: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        let results = calculate(data: data)
        let format = "\t%@\t|\t%@\t|\t%@\t|\t%@"
        print("--------------------------------------------------------------")
        print(String(format: format, "№", "Верно?", "Возможных ходов", "Сколько бит"))
        print("--------------------------------------------------------------")
        for result in results {
            let resultSteps = "\t\t\(result.steps)\t"
            let resultBits = "\(result.resultBits)"
            print(String(format: format, "\(result.numberOfTest)", result.resultBool, resultSteps, resultBits))
        }
        print("--------------------------------------------------------------")
    }
}

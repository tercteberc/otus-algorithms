//
//  BitboardParser.swift
//  Algorithms
//
//  Created by Anton Lomakin on 02/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public struct BitboardParserReadRules: ReadRules {
    
    private struct Rule: ReadRule {
        var rule: String
        
        func processing(string: String) -> [String] {
            var comp = components(of: string, separetedBy: self.rule)
            if let last = comp.last {
                comp.removeLast()
                let normalLast = last.replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\r", with: "")
                comp.append(normalLast)
            }
            return comp
        }
    }
    
    public let inputRule: ReadRule = Rule(rule: "/")
    public let outputRule: ReadRule = Rule(rule: "\r\n")
}

fileprivate struct Result {
    let numberOfTest: Int // порядковый номер теста
    let resultBool: String // пройден ли тест?
}

private protocol BitPieceValue {
    
//    var bitValue:
}

// Класс парсящий FEN-нотации
public final class BitboardParser {
    
    enum Piece: String, CaseIterable {
        
        case whitePawns = "P"   // Белая Пешка
        case whiteKnights = "N" // Белый Конь
        case whiteBishops = "B" // Белый Слон
        case whiteRooks = "R"   // Белая Ладья
        case whiteQueens = "Q"  // Белая Королева
        case whiteKing = "K"    // Белый Король
        
        case blackPawns = "p"   // Черная Пешка
        case blackKnights = "n" // Черный Конь
        case blackBishops = "b" // Черный Слон
        case blackRooks = "r"   // Черная Ладья
        case blackQueens = "q"  // Черная Королева
        case blackKing = "k"    // Черный Король
    }
    
    private func calculate(data: [ExampleTest]) -> [Result] {
        var result: [Result] = []
        for testData in data {
            let bitboard = parseBitboard(inputs: testData.data.inputs)
            result.append(checkTest(testNumber: testData.numberOfTest,
                                    test: testData.data.outputs,
                                    forBitboard: bitboard))
        }
        return result
    }
    
    private func parseBitboard(inputs: [String]) -> [Piece: UInt64] {
        let reversed = inputs.reversed()
        var results: [Piece: UInt64] = [:]
        
        var bits: UInt64 = 0
        for input in reversed {
            for i in 0 ..< input.count {
                let char = input[input.index(input.startIndex, offsetBy: i)]
                if let number = UInt64(String(char)) {
                    bits += number
                } else if let figure = Piece(rawValue: String(char)) {
                    let value = UInt64(results[figure] ?? 0)
                    let currentValue = UInt64(1) << bits
                    results[figure] = value + currentValue
                    bits += 1
                }
            }
        }
        return results
    }
    
    private func checkTest(testNumber: Int, test: [String], forBitboard: [Piece: UInt64]) -> Result {
        var result = true
        for (index, value) in Piece.allCases.enumerated() {
            if let resultBit = forBitboard[value], String(resultBit) == test[index] {
                continue
            } else if test[index] == "0" {
                continue
            }
            result = false
            break
        }
        return .init(numberOfTest: testNumber, resultBool: String(result).uppercased())
    }
    
}

extension BitboardParser: Executable {
    
    public func executeTask(for data: [ExampleTest]) {
        let results = calculate(data: data)
        let format = "\t%@\t|\t%@\t"
        print("---------------------")
        print(String(format: format, "№", "Верно?"))
        print("---------------------")
        for result in results {
            print(String(format: format, "\(result.numberOfTest)", result.resultBool))
        }
        print("---------------------")
    }
}

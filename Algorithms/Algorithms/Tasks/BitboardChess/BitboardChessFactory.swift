//
//  BitboardChessFactory.swift
//  Algorithms
//
//  Created by Anton Lomakin on 01/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public final class BitboardChessFactory: Factoriable {
    
    private enum Task: String, CaseIterable, TaskInterface {
        
        case king = "Прогулка короля"
        case knight = "Прогулка коня"
        case parse_fen = "Парс FEN-нотаций"
        case cancel = "Назад"
        
        init?(rawValue: String) {
            guard let intValue = Int(rawValue) else { return nil }
            switch intValue {
            case 0: self = .king
            case 1: self = .knight
            case 2: self = .parse_fen
            default: self = .cancel
            }
        }
        
        var testPath: String {
            switch self {
            case .king: return "/BitboardKing"
            case .knight: return "/BitboardKnight"
            case .parse_fen: return "/FEN"
            default: return ""
            }
        }
        
        var executedTask: Executable? {
            switch self {
            case .king: return BitboardWalks(fugire: .king)
            case .knight: return BitboardWalks(fugire: .knight)
            case .parse_fen: return BitboardParser()
            default: return nil
            }
        }
        
        var parseRules: ReadRules {
            switch self {
            case .king,
                 .knight,
                 .cancel:
                return BitboardWalksReadRules()
            case .parse_fen:
                return BitboardParserReadRules()
            }
        }
    }
    
    private var currentTask: Task?
    
    // MARK: - Private
    
    private func showTasksDialog(for testsPath: String) {
        while currentTask != .cancel {
            print("\nДоступные задания для тестов:")
            for (index, value) in Task.allCases.enumerated() {
                print("\t\(index): \(value.rawValue)")
            }
            print("\nТестировать будем задание №: ")
            startTask(for: readLine(), in: testsPath)
        }
    }
    
    private func startTask(for result: String?, in testsPath: String) {
        guard let string = result, let task = Task(rawValue: string) else { return }
        if let test = task.executedTask {
            print("\n\n\(task.rawValue). Результат тестов:")
            let path = testsPath + task.testPath
            test.executeTask(for: TestExampleReader.data(for: path, by: task.parseRules))
        } else {
            currentTask = task
        }
    }
    
    // MARK: Public
    
    public func startFabric(for testsPath: String) {
        showTasksDialog(for: testsPath)
    }
}

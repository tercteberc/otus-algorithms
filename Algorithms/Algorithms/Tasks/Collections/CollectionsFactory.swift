//
//  CollectionsFactory.swift
//  Algorithms
//
//  Created by Anton Lomakin on 03/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public final class CollectionsFactory {
    
    func test() {
        print("Start test")
        let singleArray = SingleArray<String>()
        addValues(array: singleArray, count: 1_000)
    }
    
    private func addValues(array: SingleArray<String>, count: Int) {
        let startTime = CFAbsoluteTimeGetCurrent()
        for num in stride(from: 0, to: count, by: 1) {
            array.add("\(num)")
        }
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        print("Add values for count: \(count), time: \(timeElapsed) sek")
    }
}

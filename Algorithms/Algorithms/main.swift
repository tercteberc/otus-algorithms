//
//  main.swift
//  Algorithms
//
//  Created by Anton Lomakin on 23/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

var directory = "/Users/Anton/Desktop/OTUS-Algo/Tests"

#if !DEBUG
print("Укажите директорию с тестами заданий.\nПо умолчанию - \(directory)\nДиректория:")

if let newDirectory = readLine(), !newDirectory.isEmpty {
    directory = newDirectory
}
#endif

//ConsoleDialog().start(with: directory)
CollectionsFactory().test()

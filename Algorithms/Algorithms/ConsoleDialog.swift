//
//  ConsoleDialog.swift
//  Algorithms
//
//  Created by Anton Lomakin on 23/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public final class ConsoleDialog {
    
    private enum MainDialog: String, CaseIterable {
        case run_examples = "Протестировать задание"
        case exit = "Выход"
        
        init?(rawValue: String) {
            guard let intValue = Int(rawValue) else { return nil }
            switch intValue {
            case 0: self = .run_examples
            case 1: self = .exit
            default: return nil
            }
        }
    }
    
    private var mainCase: MainDialog = .run_examples
    
    // MARK: - Private
    
    private func updateMainDialog(for result: String?, in directory: String) {
        guard let string = result, let dialogCase = MainDialog(rawValue: string) else { return }
        if dialogCase == .run_examples {
            TestFactory().showDialog(with: directory)
        } else {
            mainCase = dialogCase
        }
    }
    
    // MARK: - Public
    
    public func start(with directory: String) {
        while mainCase != .exit {
            print("\n\nВыберите действие:")
            for (index, value) in MainDialog.allCases.enumerated() {
                print("\t\(index): \(value.rawValue)")
            }
            print("\nДействие №: ")
            updateMainDialog(for: readLine(), in: directory)
        }
    }
}

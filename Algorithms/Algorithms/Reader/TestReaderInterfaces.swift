//
//  TestReaderInterfaces.swift
//  Algorithms
//
//  Created by Anton Lomakin on 02/03/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

public protocol ExampleTest {
    var numberOfTest: Int { get }
    var data: TestData { get }
}

public protocol TestData {
    var inputs: [String] { get }
    var outputs: [String] { get }
}

public protocol ReadRules {
    var inputRule: ReadRule { get }
    var outputRule: ReadRule { get }
}

public protocol ReadRule {
    var rule: String { get }
    
    func processing(string: String) -> [String]
}

extension ReadRule {
    
    public func processing(string: String) -> [String] {
        return components(of: string, separetedBy: rule)
    }
    
    public func components(of string: String, separetedBy: String) -> [String] {
        return string.components(separatedBy: separetedBy)
    }
}

//
//  TestExampleReader.swift
//  Algorithms
//
//  Created by Anton Lomakin on 23/02/2020.
//  Copyright © 2020 Anton Lomakin. All rights reserved.
//

import Foundation

private struct Example: ExampleTest {
    
    fileprivate struct ExampleData: TestData {
        let inputs: [String]
        let outputs: [String]
    }
    
    let numberOfTest: Int
    let data: TestData
}

// MARK: - Reader

public struct TestExampleReader {
    
    private static let inputSuffix = ".in"
    private static let outputSuffix = ".out"
    
    // MARK: - Private
    
    private static func parseElement(_ element: String,
                                     by path: String,
                                     in dictionary: inout [Int: String]) {
        if let rangeKey = element.rangeOfCharacter(from: CharacterSet.decimalDigits),
            let key = Int(String(element[rangeKey])) {
            
            let endPath = path + "/" + element
            if let content = try? NSString(contentsOfFile: endPath, usedEncoding: nil) as String {
                dictionary[key] = content
            }
        }
    }
    
    private static func createData(by rules: ReadRules, input: [Int: String], output: [Int: String]) -> [ExampleTest] {
        var result: [Example] = []
        input.forEach({
            if let outputValue = output[$0.key] {
                let exampleData = Example.ExampleData(inputs: rules.inputRule.processing(string: $0.value),
                                                      outputs: rules.outputRule.processing(string: outputValue))
                result.append(Example(numberOfTest: $0.key, data: exampleData))
            }
        })
        return result
    }
    
    // MARK: - Public
    
    public static func data(for directoryPath: String, by rules: ReadRules) -> [ExampleTest] {
        let fileManager = FileManager.default
        let enumerator = fileManager.enumerator(atPath: directoryPath)
        guard let allObjects = enumerator?.allObjects as? [String] else { return [] }
        
        var inputTests: [Int: String] = [:]
        var outputTests: [Int: String] = [:]
        
        for element in allObjects {
            if element.hasSuffix(TestExampleReader.inputSuffix) {
                parseElement(element, by: directoryPath, in: &inputTests)
            }
            if element.hasSuffix(TestExampleReader.outputSuffix) {
                parseElement(element, by: directoryPath, in: &outputTests)
            }
        }
        let exampleTests = createData(by: rules, input: inputTests, output: outputTests)
        return exampleTests.sorted(by: { $0.numberOfTest < $1.numberOfTest })
    }
}
